package com.example.movieticket_booking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class Signup extends AppCompatActivity {
    Button register;
    EditText fullname,Email,phonenum,passwd;

    FirebaseAuth auth;

    DatabaseReference myref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        fullname=(EditText) findViewById(R.id.username);
        Email=(EditText) findViewById(R.id.email);
        phonenum=(EditText) findViewById(R.id.phone);
        passwd=(EditText) findViewById(R.id.password1);
        register= (Button) findViewById(R.id.Register);

        auth=FirebaseAuth.getInstance();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String name_Text = fullname.getText().toString();
               String U_email=Email.getText().toString();
               String U_num=phonenum.getText().toString();
               String U_passwrd=passwd.getText().toString();

               // if(passwd.length()>=8){
                   // passwd.setError("Password show be min 8  Charater!!");
                 //   return;
                //}
                if(phonenum.length()!=10){
                    phonenum.setError("check the phoneNumber!!");
                    return;
                }
               if(TextUtils.isEmpty(name_Text)||TextUtils.isEmpty(U_email)||TextUtils.isEmpty(U_num)||TextUtils.isEmpty(U_passwrd)){
                   Toast.makeText(Signup.this, "Please Fill All Fields", Toast.LENGTH_SHORT).show();
               }else{
                   RegisterHere(name_Text,U_email,U_num,U_passwrd);
               }
            }
        });

    }
    private void RegisterHere(final String username,String email,String password,String phonenumber){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser firebaseUser= auth.getCurrentUser();
                            String userid=firebaseUser.getUid();
                            myref = FirebaseDatabase.getInstance()
                                    .getReference("MyUsers")
                                    .child(userid);

                            HashMap<String,String> hashMap= new HashMap<>();
                            hashMap.put("id",userid);
                            hashMap.put("fullname",username);
                            hashMap.put("Email",email);
                            hashMap.put("Password",password);
                            hashMap.put("Contact",phonenumber);

                            myref.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Intent i =new Intent(Signup.this,location.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                        Toast.makeText(Signup.this, "Succesfully Registered", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                }
                            });
                        }else{
                            Toast.makeText(Signup.this, "Invalid Email and Password", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}