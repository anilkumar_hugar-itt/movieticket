package com.example.movieticket_booking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.movieticket_booking.Model.Userlocation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class location extends AppCompatActivity {
    Button location;
    Spinner spinner;
    DatabaseReference Location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        spinner = findViewById(R.id.spinner);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Bangalore");
        arrayList.add("Belagavi");
        arrayList.add("Mysore");
        arrayList.add("Kalaburgi");
        arrayList.add("Balari");
        arrayList.add("Tumkur");


        Location= FirebaseDatabase.getInstance().getReference().child("Userlocation");

        location=(Button)findViewById(R.id.locationButton);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loc=new Intent(location.this,index.class);
                startActivity(loc);
                insertUserLocation();
                finish();
            }
        });
    }

    private void insertUserLocation() {
        String LOC=spinner.getOnItemSelectedListener().toString();

        Userlocation userlocation=new Userlocation(LOC);
        Location.push().setValue(userlocation);
        Toast.makeText(location.this, "Location enter", Toast.LENGTH_SHORT).show();

    }
}