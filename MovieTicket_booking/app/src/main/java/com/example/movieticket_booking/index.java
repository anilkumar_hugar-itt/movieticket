package com.example.movieticket_booking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class index extends AppCompatActivity {
    FirebaseAuth auth;
    DatabaseReference myref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        RecyclerView recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        MovieData[] myMoviedata=new MovieData[]{
                new MovieData("RRR","2022 film",R.drawable.rrr),
                new MovieData("GanguBai","2022 film",R.drawable.gangubai),
                new MovieData("Kashmir Files","2022 film",R.drawable.kashmir),
                new MovieData("RadheShyam","2022 film",R.drawable.radheshyam),
                new MovieData("James","2022 film",R.drawable.james),
                new MovieData("BlackLight","2022 film",R.drawable.blacknight),
                new MovieData("The Desperate Hour","2022 film",R.drawable.desperatehour),
        };
        MyMovieAdapter myMovieAdapter=new MyMovieAdapter(myMoviedata,index.this);
        recyclerView.setAdapter(myMovieAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.Logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent= new Intent(index.this,login.class);
                startActivity(intent);
                break;
            case R.id.profile:
                Intent profile=new Intent(index.this,profile.class);
                startActivity(profile);
                break;


        }
        return super.onOptionsItemSelected(item);
    }

}