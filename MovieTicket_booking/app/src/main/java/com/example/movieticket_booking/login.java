package com.example.movieticket_booking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity {
    Button login,register;
    FirebaseAuth auth;
    FirebaseUser firebaseUser;
    EditText usermail,userpassword;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usermail=(EditText)findViewById(R.id.usernsme);
        userpassword=(EditText) findViewById(R.id.password1);
        login = (Button) findViewById(R.id.submit);
        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reg2=new Intent(login.this,Signup.class);
                startActivity(reg2);
                finish();
            }
        });

        auth= FirebaseAuth.getInstance();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String u_email=usermail.getText().toString();
                String u_passwd=userpassword.getText().toString();

                if(usermail.getText().toString().isEmpty()){
                    usermail.setError("Please check your email");
                    return;
                }

               // if(usermail.getText().toString().trim().matches(emailPattern)){
                   // usermail.setError("enter correct email");
                //    return;
               // }
                if(userpassword.getText().toString().isEmpty() || userpassword.length()>=8){
                    usermail.setError("Please check your password");
                    return;
                }

                auth.signInWithEmailAndPassword(u_email,u_passwd).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Intent sucess=new Intent(login.this,location.class);
                        startActivity(sucess);
                        Toast.makeText(login.this, "login Successful", Toast.LENGTH_SHORT).show();


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(login.this, "Enter Correct Email and Password", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });



    }
    protected void onStart(){
        super.onStart();
        if(FirebaseAuth.getInstance().getCurrentUser()!=null){
            startActivity(new Intent(getApplicationContext(),location.class));
            finish();
        }

    }
}