package com.example.movieticket_booking;

import androidx.appcompat.app.AppCompatActivity;

import android.app.UiAutomation;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.QuickContactBadge;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button b1;
    Button b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button) findViewById(R.id.Sign_up);
        b2 = (Button) findViewById(R.id.login);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Sign_up:
                Intent sign_up = new Intent(this,Signup.class);
                startActivity(sign_up);
                break;

            case R.id.login:
                Intent login= new Intent(this,login.class);
                startActivity(login);
                break;
        }

    }
}