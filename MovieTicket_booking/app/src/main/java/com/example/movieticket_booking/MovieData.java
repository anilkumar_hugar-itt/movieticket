package com.example.movieticket_booking;

public class MovieData {
    String MovieName;
    String Date;
    Integer movieImage;

    public MovieData(String movieName,String date,Integer MovieImage) {
        this.MovieName = movieName;
        this.Date=date;
        this.movieImage=MovieImage;
    }

    public String getMovieName() {
        return MovieName;
    }

    public void setMovieName(String movieName) {
        MovieName = movieName;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Integer getMovieImage() {
        return movieImage;
    }

    public void setMovieImage(Integer movieImage) {
        this.movieImage = movieImage;
    }
}
